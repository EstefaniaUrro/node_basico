const http = require('http');
const qs = require('querystring');          //<----------------------------------
const func = require('./conversion');

const hostname = 'localhost';
const port = 3006;
const informe = {
    celsiusI: 0,
    fahrenheitO: ""
}
const informe1 = {
    kilometrosI: 0,
    millasO: ""
}

var formulario = `<form method="POST" action="/procesar">
<h1>Los datos</h1> 
<fieldset>
<label>Temperatura (C)</label>
<input type="number" name="centigrados" value={{1}} /> {{2}}
</fieldset>
<input type="submit" value="Enviar" />
</form>`;

var formulario1 = `<form method="<POST" action="/procesar1">
<h1>Los datos</h1> 
<fieldset>
<label>Velocidad (Km)</label>
<input type="number" name="killometros" value={{1}} /> {{2}}
</fieldset>
<input type="submit" value="Enviar" />
</form>`;

/**
 * Creacion de las opciones y metodos del servidor
 */
const server = http.createServer((req, res) => {
    if (req.method == "GET") {
        dialogoGET(req, res)
    } else {
        if (req.method == 'POST') {
            dialogoPOST(req, res)
        } else {
            console.log(req.headers);
            res.statusCode = 500;
            res.setHeader('Content-Type', 'text/html');
            res.end('Metodo desconocido=' + req.method);
        }
    }
})
/**
 * Arranque del servidor
 */
server.listen(port, hostname, () => {
    console.log(`Servidor activo en http://${hostname}:${port}/`);
});

function dialogoGET(req, res) {
    if ('/f2c' == req.url) {
        enviaFormulario(res, informe);
    }
    if ('/m2km' == req.url) {
        enviaFormulario1(res, informe1);
    }
}

function dialogoPOST(req, res) {
    console.log("POST reconocido", req);
    var body = [];
    req.on('data', trozo => {
        body.push(trozo);
        console.log("trozo=" + trozo);
    }).on('end', () => {
        body = Buffer.concat(body).toString()
    });
    console.log("body=" + body);
    req.on('end', function () {
        console.log(body);
        if ('/procesar' == req.url) {
            console.log("Url reconocido");
            informe.celsiusI = qs.parse(body).centigrados;
            informe.fahrenheitO = "Convertido a F, es: " + func.f2c(informe.celsiusI);
            enviaFormulario(res, informe);
        }
        if ('/procesar1' == req.url) {
            console.log("Url reconocido");
            informe1.kilometrosI = qs.parse(body).kilometros;
            informe1.millasO = "Convertido a millas, es: " + func.m2km(informe1.kilometrosI);
            enviaFormulario1(res, informe1);
        }
    })
}

function enviaFormulario(res, informe) {
    console.log(informe);
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    salida = formulario;
    salida = salida.replace("{{1}}", informe.celsiusI);
    salida = salida.replace("{{2}}", informe.fahrenheitO);
    res.end(salida);
}
function enviaFormulario1(res, informe1) {
    console.log(informe1);
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    salida = formulario1;
    salida = salida.replace("{{1}}", informe.kilometrosI);
    salida = salida.replace("{{2}}", informe.millasO);
    res.end(salida);
}