const FACTOR_MILLAS_KM = 1.60934;
module.export = {
    m2km: function (millas) {
        return millas * FACTOR_MILLAS_KM;
    },
    km2m2: function (km) {
        return millas / FACTOR_MILLAS_KM;
    },
    c2f: function (c) {
        let cToF = c * 9 / 5 + 32;
        return cToF;
    },
    f2c: function (f) {
        let fToC = (f - 32) * 5 / 9;
        return fToC;
    }

}
